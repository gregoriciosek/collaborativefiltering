package Data;

import java.util.ArrayList;

public class Product {
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public double getProductSimilarityMultiplicationRate(ArrayList<Client> clients, Product product){
        double rate = 0;
        int bothGotIt;
        for(Client client : clients){
            bothGotIt = 0;
            for(Long productId : client.getBoughtProducts()){
                if(this.getId() == productId || product.getId() == productId)
                   bothGotIt ++;
            }
            if(bothGotIt == 2)
                rate++;
        }
        return rate;
    }

    public double getVectorLength(ArrayList<Client> clients){
        double result = 0;
        for(Client client : clients){
            for(Long productId : client.getBoughtProducts()){
                if(productId == this.getId())
                    result ++;
            }
        }
        return result == 0 ? 0 : Math.sqrt(result);
    }
}

