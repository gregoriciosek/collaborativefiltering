package Data;

import java.util.ArrayList;

public class Client {

    private long id;
    private String name;
    private ArrayList<Long> boughtProducts;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Long> getBoughtProducts() {
        return boughtProducts;
    }

    public Client(long id, String name){
        this.id = id;
        this.name = name;
        this.boughtProducts = new ArrayList<>();
    }

    public void addBougthProduct(long id){
        this.boughtProducts.add(id);
    }

    public double calculateSquareSum(){
        double sum = 0;
        for (int i = 0; i<boughtProducts.size(); i++) {
            sum += 1;
        }
        return sum;
    }

    public double calculateVectorLength(){
        return Math.sqrt(calculateSquareSum());
    }

    public double getClientsSimilarityMultiplicationValue(Client client){
        double result = 0;
        for(long clientBougthItemId : client.getBoughtProducts()){
            for(long myBougthItemId : this.getBoughtProducts()){
                if(myBougthItemId == clientBougthItemId)
                    result += 1;
            }
        }
        return result;
    }

    public boolean wasBougth(long productId){
        for(long l : boughtProducts){
            if(productId == l){
                return true;
            }
        }
        return false;
    }
}
