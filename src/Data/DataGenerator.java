package Data;

/*

   P1P2P3P4P5P6P7P8P9
K1 1 0 0 1 0 1 1 1 1
K2 1 0 0 1 0 0 0 1 1
K3 0 0 0 0 0 0 1 0 0
K4 0 0 1 1 1 0 0 0 0

 */

import java.util.ArrayList;

public class DataGenerator {

    public DataGenerator(){}

    public ArrayList<Product> generateProducts(){

        /* Generate products */
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product(1,"P1"));
        products.add(new Product(2,"P2"));
        products.add(new Product(3,"P3"));
        products.add(new Product(4,"P4"));
        products.add(new Product(5,"P5"));
        products.add(new Product(6,"P6"));
        products.add(new Product(7,"P7"));
        products.add(new Product(8,"P8"));
        products.add(new Product(9,"P9"));

        return products;
    }

    public ArrayList<Client> generateClients(){

        /* Generate clients */
        ArrayList<Client> clients = new ArrayList<>();
        clients.add(new Client(1,"K1"));
        clients.add(new Client(2,"K2"));
        clients.add(new Client(3,"K3"));
        clients.add(new Client(4,"K4"));

        /* Add bougth products to clients */
        clients.get(0).addBougthProduct(1);
        clients.get(0).addBougthProduct(4);
        clients.get(0).addBougthProduct(6);
        clients.get(0).addBougthProduct(7);
        clients.get(0).addBougthProduct(8);
        clients.get(0).addBougthProduct(9);
        clients.get(1).addBougthProduct(1);
        clients.get(1).addBougthProduct(4);
        clients.get(1).addBougthProduct(8);
        clients.get(1).addBougthProduct(9);
        clients.get(2).addBougthProduct(7);
        clients.get(3).addBougthProduct(3);
        clients.get(3).addBougthProduct(4);
        clients.get(3).addBougthProduct(5);

        return clients;
    }
}
