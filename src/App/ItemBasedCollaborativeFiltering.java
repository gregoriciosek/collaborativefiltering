package App;

import Data.Client;
import Data.Product;
import Utils.ProductsOperations;

import java.util.ArrayList;
import java.util.HashMap;

public class ItemBasedCollaborativeFiltering {

    private ArrayList<Product> products;
    private ArrayList<Client> clients;
    private ProductsOperations productsOperations = new ProductsOperations();

    public ItemBasedCollaborativeFiltering(ArrayList<Product> products, ArrayList<Client> clients) {
        this.products = products;
        this.clients = clients;
    }

    public void doFilter(int choosenProduct, int likelyProductsCount, int recomendedProductsCount){
        /* Create Item Similarity Matrix for Choosen Product */
        HashMap<Long, Double> itemSimilarityMatrix = productsOperations.createISM(clients, products, choosenProduct);
        /* Getting best N likely products for Choosen Product */
        HashMap<Long, Double> bestNSimilarProducts = productsOperations.getBestNSimilarProducts(itemSimilarityMatrix, likelyProductsCount);
        /* Getting best N recomended clients for Choosen Product */
        HashMap<Long, Double> recomendedClients = productsOperations.getNRecomendedClients(products,clients,recomendedProductsCount, choosenProduct,bestNSimilarProducts);

        System.out.println("Wybrany produkt : " + (choosenProduct+1));

        /* Printing ISM for Choosen Product */
        itemSimilarityMatrix.forEach((aLong, aDouble) -> {
            System.out.println("Product " + aLong + ", Rate : " + aDouble);
        });

        /* Printing best N likely products for Choosen Product */
        System.out.println("Wielkość grupy wsparcia : " + likelyProductsCount);
        bestNSimilarProducts.forEach((aLong, aDouble) -> {
            System.out.println("Product " + aLong + ", Rate : " + aDouble);
        });

        System.out.println("Ilośc rekomendowanych przedmiotów : " + recomendedProductsCount);
        recomendedClients.forEach((aLong, aDouble) -> {
            System.out.println("Client " + aLong + ", Rate : " + aDouble);
        });
    }
}
