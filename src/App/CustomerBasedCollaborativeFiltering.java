package App;

import Data.Client;
import Data.Product;
import Utils.ClientsOperations;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomerBasedCollaborativeFiltering {

    private ArrayList<Product> products;
    private ArrayList<Client> clients;
    private ClientsOperations clientOperations = new ClientsOperations();

    public CustomerBasedCollaborativeFiltering(ArrayList<Product> products, ArrayList<Client> clients) {
        this.products = products;
        this.clients = clients;
    }

    public void doFilter(int choosenClient, int likelyClientsCount, int recomendedProductsCount){
        /* Create Customer Similarity Matrix for Choosen Client */
        HashMap<Long, Double> customerSimilarityMatrix = clientOperations.createCSM(clients, choosenClient);
        /* Getting best N likely clients for Choosen Client */
        HashMap<Long, Double> bestNSimilarCustomers = clientOperations.getBestNSimilarClients(customerSimilarityMatrix, likelyClientsCount);
        /* Getting best N recomended products for Choosen Client */
        HashMap<Long, Double> recomendedProducts = clientOperations.getNRecomendedProducts(products,clients,recomendedProductsCount, choosenClient,bestNSimilarCustomers);

        System.out.println("Wybrany klient : " + choosenClient);

        /* Printing CSM for Choosen Client */
        customerSimilarityMatrix.forEach((aLong, aDouble) -> {
            System.out.println("Client " + aLong + ", Rate : " + aDouble);
        });

        /* Printing best N likely clients for Choosen Client */
        System.out.println("Wielkość grupy wsparcia : " + likelyClientsCount);
        bestNSimilarCustomers.forEach((aLong, aDouble) -> {
            System.out.println("Client " + aLong + ", Rate : " + aDouble);
        });

        System.out.println("Ilośc rekomendowanych przedmiotów : " + recomendedProductsCount);
        recomendedProducts.forEach((aLong, aDouble) -> {
            System.out.println("Product " + aLong + ", Rate : " + aDouble);
        });
    }
}
