package App;

import Data.Client;
import Data.DataGenerator;
import Data.Product;

import java.util.ArrayList;

public class Engine {

    private int choosenClient = 2;
    private int likelyClientsCount = 2;

    private int choosenProduct = 6;
    private int likelyProductsCount = 2;


    private int recomendedProductsCount = 2;
    private int recomendedClientsCount = 2;

    private DataGenerator dataGenerator = new DataGenerator();

    public Engine(){}

    public void engineStart(){

    ArrayList<Product> products = dataGenerator.generateProducts();
    ArrayList<Client> clients = dataGenerator.generateClients();

    System.out.println("Customer-Based Collaborative Filtration : ");
    CustomerBasedCollaborativeFiltering customerBasedCollaborativeFiltering = new CustomerBasedCollaborativeFiltering(products,clients);
    customerBasedCollaborativeFiltering.doFilter(choosenClient,likelyClientsCount,recomendedProductsCount);

    System.out.println("\n\n\nItem-Based Collaborative Filtration : ");
    ItemBasedCollaborativeFiltering itemBasedCollaborativeFiltering = new ItemBasedCollaborativeFiltering(products,clients);
    itemBasedCollaborativeFiltering.doFilter(choosenProduct-1, likelyProductsCount, recomendedClientsCount);

    }
}
