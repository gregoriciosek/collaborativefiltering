package Utils;

import Data.Client;
import Data.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductsOperations {

    public ProductsOperations(){}

    public HashMap<Long, Double> createISM(ArrayList<Client> clients, ArrayList<Product> products, int choosenProduct){
        HashMap<Long, Double> itemSimilarityMatrix = new HashMap<>();
        double isr = 0;
        double vectorsRatio = 0;
        for(Product product : products){
            if((choosenProduct+1)!=product.getId()) {
                isr = products.get(choosenProduct).getProductSimilarityMultiplicationRate(clients, product);
                vectorsRatio = product.getVectorLength(clients)*products.get(choosenProduct).getVectorLength(clients);
                if(vectorsRatio==0) {
                    isr = 0;
                    itemSimilarityMatrix.put(product.getId(), isr);
                }
                else
                    itemSimilarityMatrix.put(product.getId(), (isr/vectorsRatio));
            }
        }
        return itemSimilarityMatrix;
    }

    public HashMap<Long, Double> getBestNSimilarProducts (HashMap<Long, Double> iSM, int n){
        long id;
        double rate;
        HashMap<Long, Double> bestNSimilarProducts = new HashMap<>();
        /* Clone CSM to delete some records during ops */
        HashMap<Long, Double> copiedISM = (HashMap<Long, Double>) iSM.clone();
        for(int i = 0; i < n ; i++){
            id = 0;
            rate = 0;
            for (Map.Entry<Long, Double> entry : copiedISM.entrySet()) {
                Long aLong = entry.getKey();
                Double aDouble = entry.getValue();
                if (rate < aDouble) {
                    id = aLong;
                    rate = aDouble;
                }
            }
            bestNSimilarProducts.put(id,rate);
            copiedISM.remove(id);
        }
        return bestNSimilarProducts;
    }

    public HashMap<Long, Double> createAllCustomersRatingForMostSimilarProducts(ArrayList<Product> products, HashMap<Long,Double> bestNSimilarProducts, ArrayList<Client> clients){
        HashMap<Long, Double> allClientsRating = new HashMap<>();
        for (Client client : clients) {
            double rate = 0;
            for (Map.Entry<Long, Double> entry : bestNSimilarProducts.entrySet()) {
                Long aLong = entry.getKey();
                Double aDouble = entry.getValue();
                if(client.wasBougth(aLong)) {
                    rate += aDouble;
                }
            }
            rate = rate / bestNSimilarProducts.size();
            allClientsRating.put(client.getId(), rate);
        }
        return allClientsRating;
    }

    public HashMap<Long, Double> getNRecomendedClients(ArrayList<Product> products, ArrayList<Client> clients, int productsCount ,int choosenProduct, HashMap<Long, Double> bestNSimilarProducts){

        HashMap<Long, Double> allClientsRating = createAllCustomersRatingForMostSimilarProducts(products,bestNSimilarProducts,clients);

        HashMap<Long, Double> recomendedClients = new HashMap<>();
        long bestId;
        double bestRate;
        /* Taking best rating clients */
        for(int i = 0; i < productsCount; i++){
            bestId = 0;
            bestRate = 0;
            for (Map.Entry<Long, Double> entry : allClientsRating.entrySet()) {
                Long aLong = entry.getKey();
                Double aDouble = entry.getValue();
                if(!clients.get(aLong.intValue()-1).wasBougth(choosenProduct+1)) { /* Filter if choosen item wasn't already bougth by client */
                    if (bestRate < aDouble) {
                        bestId = aLong;
                        bestRate = aDouble;
                    }
                }
            }
            recomendedClients.put(bestId,bestRate);
            allClientsRating.remove(bestId);
        }
        return recomendedClients;
    }

}
