package Utils;

import Data.Client;
import Data.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ClientsOperations {

    public ClientsOperations(){}

    public HashMap<Long, Double> createCSM(ArrayList<Client> clients, int choosenClient){
        HashMap<Long, Double> customerSimilarityMatrix = new HashMap<>();
        for(Client client : clients){
            if(client.getId() != choosenClient) {
                double csr = getClientSimilarityRate(clients.get(choosenClient - 1),client);
                customerSimilarityMatrix.put( client.getId(), csr );
            }
        }
        return customerSimilarityMatrix;
    }

    public double getClientSimilarityRate(Client clientA, Client clientB){
        double result = 0;
        result += (clientA.getClientsSimilarityMultiplicationValue(clientB)) / (clientA.calculateVectorLength() * clientB.calculateVectorLength());
        return result;
    }

    public HashMap<Long, Double> getBestNSimilarClients (HashMap<Long, Double> cSM, int n){
        long id;
        double rate;
        HashMap<Long, Double> bestNSimilarClients = new HashMap<>();
        /* Clone CSM to delete some records during ops */
        HashMap<Long, Double> copiedCSM = (HashMap<Long, Double>) cSM.clone();
        for(int i = 0; i < n ; i++){
            id = 0;
            rate = 0;
            for (Map.Entry<Long, Double> entry : copiedCSM.entrySet()) {
                Long aLong = entry.getKey();
                Double aDouble = entry.getValue();
                if (rate < aDouble) {
                    id = aLong;
                    rate = aDouble;
                }
            }
            bestNSimilarClients.put(id,rate);
            copiedCSM.remove(id);
        }
        return bestNSimilarClients;
    }

    public HashMap<Long, Double> createAllProductsRatingForMostSimilarCustomers(ArrayList<Product> products, HashMap<Long,Double> bestNSimilarCustomers, ArrayList<Client> clients){
        HashMap<Long, Double> allProductsRating = new HashMap<>();
        for (Product product : products) {
            double rate = 0;
            for (Map.Entry<Long, Double> entry : bestNSimilarCustomers.entrySet()) {
                Long aLong = entry.getKey();
                Double aDouble = entry.getValue();
                if(clients.get(aLong.intValue()-1).wasBougth(product.getId())) {
                    rate += aDouble;
                }
            }
            rate = rate / bestNSimilarCustomers.size();
            allProductsRating.put(product.getId(), rate);
        }
        return allProductsRating;
    }

    public HashMap<Long, Double> getNRecomendedProducts(ArrayList<Product> products, ArrayList<Client> clients, int productsCount ,int choosenClient, HashMap<Long, Double> bestNSimilarCustomers){

        HashMap<Long, Double> allProductsRating = createAllProductsRatingForMostSimilarCustomers(products,bestNSimilarCustomers,clients);

        HashMap<Long, Double> recomendedProducts = new HashMap<>();
        long bestId;
        double bestRate;
        /* Taking best rating products */
        for(int i = 0; i < productsCount; i++){
            bestId = 0;
            bestRate = 0;
            for (Map.Entry<Long, Double> entry : allProductsRating.entrySet()) {
                Long aLong = entry.getKey();
                Double aDouble = entry.getValue();
                if(!clients.get(choosenClient-1).wasBougth(aLong.intValue())) { /* Filter if item wasn't already bougth by choosen client */
                    if (bestRate < aDouble) {
                        bestId = aLong;
                        bestRate = aDouble;
                    }
                }
            }
            recomendedProducts.put(bestId,bestRate);
            allProductsRating.remove(bestId);
        }
        return recomendedProducts;
    }

}
